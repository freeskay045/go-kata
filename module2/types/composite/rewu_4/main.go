package main

import (
	"fmt"
	"math/rand"
	"sort"
	"time"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

// User создай структуру User.

type User struct {
	Name   string
	Salary int
	Age    int
}

// generateAge сгенерируй возраст от 18 до 70 лет.
func generateAge() int {
	// используй rand.Intn()
	Age := rand.Intn(90)
	return Age
}

// generateIncome сгенерируй доход от 0 до 500000.
func generateIncome() int {
	// используй rand.Intn()
	Salary := rand.Intn(500000)
	return Salary
}

// generateFullName сгенерируй полное имя. например "John Doe".
func generateFullName() string {
	// создай слайс с именами и слайс с фамилиями.
	name := []string{"Sacha", "Artem", "Vladimir", "Timur", "Alexey", "Petr"}
	sername := []string{"Vasilyev", "Mironinov", "Veremchuk", "Shvets"}
	// используй rand.Intn() для выбора случайного имени и фамилии.
	Name := name[rand.Intn(len(name)-1)] + sername[rand.Intn(len(sername)-1)]
	return Name
}

func main() {
	// Сгенерируй 1000 пользователей и заполни ими слайс users.
	n := 1000
	result2 := []User{}
	var result []User
	for i := 0; i <= n; i++ {

		users1 := User{
			Name:   generateFullName(),
			Salary: generateIncome(),
			Age:    generateAge(),
		}
		result = append(result, users1)
		result2 = result
	}

	// Выведи средний возраст пользователей.
	sr := 0
	for inx, _ := range result2 {
		sr += result2[inx].Age
	}
	sr = sr / 1000
	fmt.Println(sr)
	// Выведи средний доход пользователей.
	sl := 0
	for inx, _ := range result2 {
		sl += result2[inx].Salary
	}
	sl = sl / 1000
	fmt.Println(sl)
	// Выведи количество пользователей, чей доход превышает средний доход.
	sum := 0
	for inx, _ := range result2 {
		if result2[inx].Salary > sl {
			sum += 1
		}
	}
	fmt.Println(sum)
	sort.Slice(result2, func(i, j int) bool { return result2[i].Salary < result2[j].Salary })
	fmt.Println(result2)

	for inx := 0; inx < len(result2)/2; inx++ {
		f := len(result2) - 1 - inx
		result2[inx], result2[f] = result2[f], result2[inx]
	}
	fmt.Println(result2)

}
