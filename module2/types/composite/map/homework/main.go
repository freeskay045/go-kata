package main

import "fmt"

type Project struct {
	Name  string
	Stars int
}

func main() {
	projects := []Project{
		{
			Name:  "https://github.com/docker/compose",
			Stars: 27600,
		},

		// сюда впишите ваши остальные 12 структур
		{
			Name:  "https://github.com/types/composite",
			Stars: 27800,
		},

		{
			Name:  "https://github.com/types/bool",
			Stars: 26800,
		},

		{
			Name:  "https://github.com/types/unit",
			Stars: 27400,
		},

		{
			Name:  "https://github.com/types/unit8",
			Stars: 23800,
		},

		{
			Name:  "https://github.com/types/float32",
			Stars: 24900,
		},

		{
			Name:  "https://github.com/types/float64",
			Stars: 25600,
		},

		{
			Name:  "https://github.com/types/string",
			Stars: 23500,
		},

		{
			Name:  "https://github.com/composite/struct",
			Stars: 21900,
		},

		{
			Name:  "https://github.com/types/struct2",
			Stars: 10000,
		},

		{
			Name:  "https://github.com/projeckt/blokchain",
			Stars: 41500,
		},

		{
			Name:  "https://github.com/types/ansible",
			Stars: 25200,
		},

		{
			Name:  "https://github.com/types/thefuck",
			Stars: 65988,
		},

		{
			Name:  "https://github.com/types/httpie",
			Stars: 53255,
		},
	}

	// в цикле запишите в map
	unicProjects := make(map[string]Project)

	for k, _ := range projects {
		unicProjects[projects[k].Name] = projects[k]
	}

	// в цикле пройдитесь по мапе и выведите значения в консоль
	for k, _ := range unicProjects {
		fmt.Println(unicProjects[k])
	}
}
