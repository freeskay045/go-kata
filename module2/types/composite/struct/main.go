package main

import (
	"fmt"
	"unsafe"
)

type User struct {
	name     string
	age      int
	Wallet   Wallet
	Location Location
}

type Location struct {
	address string
	city    string
	index   string
}

type Wallet struct {
	RUR uint64
	USD uint64
	BTC uint64
	ETH uint64
}

func main() {
	user := User{
		name: "Jhon",
		age:  24,
	}

	fmt.Println(user)
	wallet := Wallet{
		RUR: 250000,
		USD: 3500,
		BTC: 1,
		ETH: 4,
	}
	user2 := User{
		age:  24,
		name: "Anton",
		Wallet: Wallet{
			RUR: 250000,
			USD: 3500,
			BTC: 1,
			ETH: 4,
		},
		Location: Location{
			address: "Push",
			city:    "Москва",
			index:   "108836",
		},
	}
	fmt.Println(user2)
	fmt.Println(wallet)
	fmt.Println("Wallet allocates: ", unsafe.Sizeof(wallet), "bytes")
	user.Wallet = wallet
	fmt.Println(user)
}
