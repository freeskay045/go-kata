package main

import "fmt"

func main() {
	a := []int{1, 98, 550, 345, 876, 12, 5670}
	fmt.Println(a)
	fmt.Println(maxAlement(a))
}

func maxAlement(n []int) int {
	var v int
	v = 0
	for _, val := range n {
		if val > v {
			v = val
		}
	}
	return v
}
