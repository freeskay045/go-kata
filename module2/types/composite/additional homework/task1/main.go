package main

import "fmt"

type Student struct {
	Name  string
	Age   int
	Grade int
}

func PrintStudent(n []Student) {
	for inx, _ := range n {
		fmt.Printf("Имя: %s, Возраст: %d\n, Класс: %d\n", n[inx].Name, n[inx].Age, n[inx].Grade)

	}
}

func main() {
	students := []Student{
		{
			Name:  "Anton",
			Age:   24,
			Grade: 5,
		},

		{
			Name:  "Sergay",
			Age:   22,
			Grade: 2,
		},

		{
			Name:  "Anya",
			Age:   23,
			Grade: 4,
		},
	}
	fmt.Println(students)
	PrintStudent(students)
}
