package main

import "fmt"

type Shape interface {
	Square()
	Perimeter()
}

type Figure struct {
	Long   int
	Width  int
	Long1  int
	Width1 int
}

func (f Figure) Square() {
	v := f.Long * f.Width
	fmt.Println(v)
}

func (f Figure) Perimeter() {
	v := f.Long1 + f.Width1 + f.Width + f.Long
	fmt.Println(v)
}

func main() {
	a := Figure{
		Long:   5,
		Width:  9,
		Long1:  4,
		Width1: 9,
	}

	a.Square()
	a.Perimeter()

}
