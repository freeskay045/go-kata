package main

import "fmt"

type Fighter interface {
	Attack()
	Defend()
}

type Knight struct {
	AttackPower int
}

type Mage struct {
	MagicPower int
}

func (k Knight) Attack() {
	fmt.Printf("Сила атаки: %d\n", k.AttackPower)
}

func (k Knight) Defend() {
	fmt.Printf("Сила защиты: %d\n", k)

}

func (m Mage) Attack() {
	fmt.Printf("Сила атаки: %d\n", m.MagicPower)

}

func (m Mage) Defend() {
	fmt.Println("ЗААААЩИИИТААААА 20000%!!!!!")

}

func main() {
	knight1 := Knight{
		AttackPower: 50,
	}

	mage1 := Mage{
		MagicPower: 70,
	}
	realaseInterface(knight1)
	realaseInterface(mage1)
}

func realaseInterface(attack Fighter) {
	attack.Attack()

}
