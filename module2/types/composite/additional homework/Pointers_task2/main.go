package main

import "fmt"

func main() {
	m := []int{4, 1, 5, 8, 7}
	minMax(&m)
	fmt.Println(m)

}

func minMax(n *[]int) {
	min, max := (*n)[0], (*n)[0]
	b := []int{}
	for _, val := range *n {
		if val < min {
			min = val
		}

		if val > max {
			max = val
		}

	}
	b = append(b, min)
	b = append(b, max)
	*n = b
}
