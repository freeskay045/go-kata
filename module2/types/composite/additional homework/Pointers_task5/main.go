package main

import "fmt"

type Person struct {
	Name string
}

func main() {
	n := Person{
		Name: "Vasya",
	}
	fmt.Println(n)
	chgPerson(&n)
	fmt.Println(n)
}

func chgPerson(n *Person) {
	*n = Person{
		Name: "Kolya",
	}

}
