package main

import "fmt"

func main() {
	sl := []int{2, 5, 8}
	Average(&sl)
	fmt.Println(sl)
}

func Average(n *[]int) {
	a := 0
	b := []int{}
	for _, val := range *n {
		a = a + val
	}
	a = a / len(*n)
	b = append(b, a)
	*n = b
}
