package main

import "fmt"

type User struct {
	Name string
	Age  int
}

type Eligible interface {
	IsEligible()
}

func (u User) IsEligible() {
	if u.GoodAge() == true {
		fmt.Println("User is eligible")
	} else {
		fmt.Println("User is not eligible")
	}
}

func (u User) GoodAge() bool {
	return u.Age >= 18
}

func printEligible(n Eligible) {
	n.IsEligible()
}

func main() {
	users := User{
		Name: "Max",
		Age:  15,
	}

	printEligible(users)
}
