package main

import "fmt"

func main() {
	s := []int{1, 4, 6, 5}
	l := []int{2, 5, 9, 4, 12}
	fmt.Println(Union(s, l))
}

func Union(s, l []int) []int {
	c := []int{}
	c = append(s, l...)
	v := make(map[int]int)
	for inx, val := range c {
		v[val] = inx
	}
	fmt.Println(v)
	n := []int{}
	for inx, _ := range v {
		n = append(n, inx)
	}
	return n
}
