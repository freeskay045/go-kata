package main

import "fmt"

func main() {
	n := 4
	x := 5
	pointersByNums(&n, &x)
	fmt.Println(n, x)
}

func pointersByNums(n, x *int) {
	*n = 5
	*x = 4

}
