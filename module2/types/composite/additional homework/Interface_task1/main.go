package main

import "fmt"

type Myinterfase interface {
}

func main() {
	var n Myinterfase
	n = 9
	fmt.Println(sort(n))

}

func sort(n interface{}) bool {
	switch n.(type) {
	case int:
		return false
	case float64:
		return false
	case string:
		return true
	}

	return false
}
