package main

import "fmt"

func main() {
	n := 6
	result(&n)
	fmt.Println(n)
}
func result(n *int) {
	*n = *n * *n
}
