package main

import "fmt"

func main() {
	sl := []int{5, 4, 7, 6}
	fmt.Println(sl)
	fmt.Println(newSlice(sl))

}
func newSlice(sl []int) []int {
	f := []int{}
	for _, val := range sl {
		if val%2 == 0 {
			f = append(f, val)
		}
	}
	return f
}
