package main

import "fmt"

type Animal struct {
	Name      string
	Kind      string
	Character Characteristic
}

type Characteristic struct {
	Name    string
	Meaning string
}

func main() {
	animals := []Animal{
		{
			Name: "tiger",
			Kind: "feline",
			Character: Characteristic{
				Name:    "Плотоядное",
				Meaning: "Ест других животных",
			},
		},

		{
			Name: "zebra",
			Kind: "artiodactyl",
			Character: Characteristic{
				Name:    "Травоядное",
				Meaning: "Ест травку и листики",
			},
		},

		{
			Name: "crocodile",
			Kind: "amphibian",
			Character: Characteristic{
				Name:    "Плотоядное",
				Meaning: "Ест других животных",
			},
		},
	}

	mapAnimals := make(map[string]Animal)

	for inx, value := range animals {
		mapAnimals[animals[inx].Name] = value
	}
	printAnimals(mapAnimals)
}

func printAnimals(mapAnimals map[string]Animal) {
	for inx, _ := range mapAnimals {
		fmt.Printf("Имя: %s, Вид: %s\n, Характеристика: %s\n, Пояснение: %s\n", mapAnimals[inx].Name, mapAnimals[inx].Kind, mapAnimals[inx].Character.Name, mapAnimals[inx].Character.Meaning)
	}
}
