package main

import "fmt"

type Transporter interface {
	Transport()
}

type Car struct {
	Name string
}

type Plane struct {
	Name string
}

func (c Car) Transport() {
	fmt.Printf("Машина %s\n доехала до...", c.Name)
}

func (p Plane) Transport() {
	fmt.Printf("Самолет %s\n долетел до...", p.Name)
}

func main() {

	carsSl := []Car{
		{
			Name: "Lada",
		},
		{
			Name: "Reno",
		},

		{
			Name: "Mersedes",
		},
	}

	plansSl := []Plane{
		{
			Name: "Boing",
		},

		{
			Name: "Il",
		},

		{
			Name: "Tu",
		},
	}

	transports := make(map[string]Transporter)

	for inx, value := range carsSl {
		transports[carsSl[inx].Name] = value
	}

	for inx, value := range plansSl {
		transports[plansSl[inx].Name] = value
	}

	fmt.Println(transports)
	printTransports(transports)
}

func printTransports(transports map[string]Transporter) {
	for _, value := range transports {
		value.Transport()
	}
}
