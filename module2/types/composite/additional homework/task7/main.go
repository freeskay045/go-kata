package main

import (
	"fmt"
	"math/rand"
	"sort"
	"time"
)

type Employee struct {
	Name       string
	Age        int
	Salary     float64
	Department string
}

type Sortable interface {
	Len() int
	Less(i, j int) bool
	Swap(i, j int)
}

type ByAge []Employee

func (a ByAge) Len() int {
	return len(a)
}

func (a ByAge) Less(i, j int) bool {
	return a[i].Name < a[j].Name
}

func (a ByAge) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}

type BySalary []Employee

func (a BySalary) Len() int {
	return len(a)
}

func (a BySalary) Less(i, j int) bool {
	return a[i].Name < a[j].Name
}

func (a BySalary) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}

type ByDepartment []Employee

func (a ByDepartment) Len() int {
	return len(a)
}

func (a ByDepartment) Less(i, j int) bool {
	return a[i].Name < a[j].Name
}

func (a ByDepartment) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}

func main() {
	employees := generateRandomEmployees(10)

	for {
		printMenu()
		var choice int
		fmt.Scan(&choice)

		switch choice {
		case 1:
			printEmployees(employees)
		case 2:
			sort.Sort(ByAge(employees))
			printEmployees(employees)
		case 3:
			sort.Sort(BySalary(employees))
			printEmployees(employees)
		case 4:
			sort.Sort(ByDepartment(employees))
			printEmployees(employees)
		case 5:
			fmt.Println("Goodbye!")
			return
		default:
			fmt.Println("Invalid choice!")
		}
	}
}

func generateRandomEmployees(n int) []Employee {
	// сгенерировать рандомных работников
	name := []string{"Vladimir", "Mariya", "Petr", "Alexander", "Nikolay", "Anya", "Sofia", "Valentina", "Darya", "Artem"}
	dep := []string{"HR", "Manager", "Sequriti", "Marketing", "IT", "Finance", "Designer", "Development", "Planing", "Sales"}
	rand.Seed(time.Now().UTC().UnixNano())
	var result []Employee
	for i := 0; i < n; i++ {
		temp := Employee{
			Name:       name[rand.Intn(len(name)-1)],
			Age:        rand.Intn(90),
			Salary:     float64(rand.Intn(100000)),
			Department: dep[rand.Intn(len(dep)-1)],
		}
		result = append(result, temp)
	}
	fmt.Println(result)
	return result
}

func printMenu() {
	fmt.Println("Please choose an option:")
	fmt.Println("1. Print all employees")
	fmt.Println("2. Sort employees by age")
	fmt.Println("3. Sort employees by salary")
	fmt.Println("4. Sort employees by department")
	fmt.Println("5. Exit")
}

func printEmployees(employees []Employee) {
	// вывести работников
	for _, value := range employees {
		fmt.Println(value)
	}
}
