package main

import (
	"fmt"
	"sort"
)

func main() {
	sl := []string{"s", "fhg", "gdtrhcbfk", "dyjk", "dt", "dgfyr", "fhgurip"}
	fmt.Println(sortSlice(sl))
}

func sortSlice(n []string) []string {
	sort.Slice(n, func(i, j int) bool {
		return len(n[i]) < len(n[j])
	})
	return n
}
