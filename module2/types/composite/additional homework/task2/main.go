package main

import "fmt"

type Student struct {
	Name    string
	Age     int
	Courses Course
}

type Course struct {
	Name        string
	Description string
	Grade       int
}

func main() {
	//course := make(map[string]Course)
	students := []Student{
		{
			Name: "Mark",
			Age:  22,
			Courses: Course{
				Name:        "Proga",
				Description: "Сложно и больно, но так приятно",
				Grade:       5,
			},
		},

		{
			Name: "Ann",
			Age:  21,
			Courses: Course{
				Name:        "Logika",
				Description: "Ее отсутствие - не конец жизни",
				Grade:       3,
			},
		},

		{
			Name: "Anton",
			Age:  23,
			Courses: Course{
				Name:        "html",
				Description: "Больше символов и тегов!",
				Grade:       4,
			},
		},
	}
	allInformation1 := make(map[string]Student)
	//allInformation2 := make(map[string]Course)

	for inx, value := range students {
		allInformation1[students[inx].Name] = value

	}
	printInformation(allInformation1)
}

func printInformation(allInformation1 map[string]Student) {
	for inx, _ := range allInformation1 {
		fmt.Printf("Имя: %s, Возраст: %d\n, Курс: %s\n, Оценка: %d\n", allInformation1[inx].Name, allInformation1[inx].Age, allInformation1[inx].Courses.Name, allInformation1[inx].Courses.Grade)
	}
}
