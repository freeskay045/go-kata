package main

import "fmt"

func main() {
	n := 5
	Sum(&n)
	fmt.Println(n)
}

func Sum(n *int) {
	*n = *n + *n

}
