package main

import "fmt"

func main() {
	sl := []string{"s", "h", "l", "h"}
	fmt.Println(originalSlice(sl))
}

func originalSlice(n []string) []string {
	m := make(map[string]int)
	for inx, val := range n {
		m[val] = inx
	}
	s := []string{}
	for inx, _ := range m {
		s = append(s, inx)
	}
	return s
}
