package main

import (
	"fmt"
)

type Keys interface {
	Key() []string
}

type Values interface {
	Value() []int
}

type Map interface {
	Keys
	Values
}

type Dictionary map[string]int

func (d Dictionary) Key() []string {
	var a []string
	for inx, _ := range d {
		a = append(a, inx)
	}
	return a
}

func (d Dictionary) Value() []int {
	var b []int
	for _, value := range d {
		b = append(b, value)
	}
	return b
}

func main() {
	var nums Dictionary
	appendAlements(&nums)
	printresult(nums)

}

func appendAlements(n *Dictionary) {
	/*str := []string{"Ноль", "Один", "Два", "Три", "Четрые", "Пять"}
	*n = make(Dictionary)
	for inx, value := range str {
		fmt.Println(inx, value) n[value] = inx
	}*/
	*n = make(Dictionary)
	*n = map[string]int{
		"Один":   1,
		"Два":    2,
		"Три":    3,
		"Четыре": 4,
		"Пять":   5,
	}

}

func printresult(n Map) {
	fmt.Println(n.Key())
	fmt.Println(n.Value())
}
