package main

import "fmt"

type Walker interface {
	Walk()
}

type Runner interface {
	Ran()
}

type Human interface {
	Walker
	Runner
}

type NameHuman string

func (n NameHuman) Walk() {
	fmt.Printf("%s идет и смотрит по сторонам\n", n)
}

func (n NameHuman) Ran() {
	fmt.Printf("%s бежит и потеет, но он собой очень доволен", n)
}

func main() {
	var name NameHuman
	defineName(&name)
	printWalker(name)

}

func printWalker(n Human) {
	n.Walk()
	n.Ran()
}

func defineName(n *NameHuman) {
	*n = "Саша"
}
