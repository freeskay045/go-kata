package main

import "fmt"

func main() {
	testArray()
	rangeArray()
}

type User struct {
	name   string
	age    int
	Wallet Wallet
}

type Wallet struct {
	RUR uint64
	USD uint64
	BTC uint64
	ETH uint64
}

func rangeArray() {
	users := [4]User{
		{
			age:  8,
			name: "Jhon",
			Wallet: Wallet{
				RUR: 13,
				USD: 1,
				BTC: 0,
				ETH: 0,
			},
		},
		{
			age:  13,
			name: "Katie",
			Wallet: Wallet{
				RUR: 500,
				USD: 3,
				BTC: 0,
				ETH: 0,
			},
		},
		{
			age:  21,
			name: "Doe",
			Wallet: Wallet{
				RUR: 0,
				USD: 300,
				BTC: 1,
				ETH: 3,
			},
		},
		{
			age:  34,
			name: "Arnie",
			Wallet: Wallet{
				RUR: 987342,
				USD: 34,
				BTC: 1,
				ETH: 3,
			},
		},
	}
	fmt.Println("Пользователи старше 18 лет:")
	for i := range users {
		if users[i].age > 18 {
			fmt.Println(users[i])

		}
	}
	fmt.Println("Пользователи, у которых есть криптовалюта на балансе:")
	for i, user := range users {
		if users[i].Wallet.BTC > 0 || users[i].Wallet.ETH > 0 {
			fmt.Println("user:", user, "index:", i)
		}

	}
	fmt.Println("========= end users non zero crypto balance ==========")
}

func testArray() {
	a := [...]int{34, 55, 89, 144}
	fmt.Println("original value", a)
	a[0] = 21
	fmt.Println("changed first value", a)
	b := a
	a[0] = 253
	fmt.Println("original array", a)
	fmt.Println("modified array", b)

}
