package main

import (
	"fmt"
	"math/rand"
	"sort"
	"time"
)

type Reverser interface {
	Reverse()
}

type Sorter interface {
	Sort()
}

type SorterReverser interface {
	Reverser
	Sorter
}

type Numbers []int

func (n Numbers) Sort() { // функция сортировки массива, использовать библиотеку sort
	sort.Ints(n)
}

func (n Numbers) Reverse() { // функция переворачивания массива, перевернуть используя метод a, b = b, a
	for inx := 0; inx < len(n)/2; inx++ {
		p := len(n) - 1 - inx
		n[inx], n[p] = n[p], n[inx]
	}
	fmt.Println(n)
}

func main() {
	var numbers Numbers
	AddNumbers(&numbers) // исправить чтобы добавляло значения
	SortReverse(numbers) // исправить чтобы сортировало и переворачивала массив
	fmt.Println(numbers)
}

func AddNumbers(s *Numbers) {
	rand.Seed(time.Now().Unix())
	for i := 0; i < 100; i++ {
		*s = append(*s, rand.Intn(100))
	}
}

func SortReverse(n SorterReverser) {
	n.Sort()
	n.Reverse()
}
