package main

import "fmt"

type User struct {
	Name string
	Age  int
}

// comment

func main() {
	users := []User{
		{
			Name: "Alice",
			Age:  21,
		},
		{
			Name: "John",
			Age:  34,
		},
		{
			Name: "Alexander",
			Age:  45,
		},
		{
			Name: "Ivan",
			Age:  13,
		},
		{
			Name: "Denis",
			Age:  44,
		},
		{
			Name: "Mary",
			Age:  26,
		},
		{
			Name: "Rose",
			Age:  41,
		},
	}
	chgUsers := make([]User, 0, 4)
	for inx, _ := range users {
		if users[inx].Age <= 40 {
			chgUsers = append(chgUsers, users[inx])
		}
	}
	fmt.Println(chgUsers)
}
