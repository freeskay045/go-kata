package main

import "fmt"

func main() {
	s := []int{1, 2, 3}
	fmt.Println(s)
	fmt.Println(Append(s))

}

func Append(s []int) []int {
	s1 := make([]int, 3, 3)
	copy(s1, s)
	s1 = append(s1, 4)
	return s1
}
