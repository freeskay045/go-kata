package main

import "fmt"

func main() {

	typeInt()
}
func typeInt() {
	var uintNumber uint8 = 1 << 7
	var from = int8(uintNumber)
	uintNumber--
	var to = int8(uintNumber)
	fmt.Println(from, to)
	var uintNumber32 uint32 = 1 << 31
	var from32 = int32(uintNumber32)
	uintNumber32--
	var to32 = int32(uintNumber)
	fmt.Println(from32, to32)
	var uintNumber64 uint64 = 1 << 63
	var from64 = int64(uintNumber64)
	uintNumber64--
	var to64 = int64(uintNumber64)
	fmt.Println(from64, to64)
}
