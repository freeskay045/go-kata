package main

import (
	"fmt"
	"unsafe"
)

func main() {
	n := 112358132134
	fmt.Println("sixe is:", unsafe.Sizeof(n), "bytes")
	typeUint()
}

func typeUint() {
	var numberUint8 uint8 = 1 << 1
	fmt.Println("Left shift uint8:", numberUint8)
	numberUint8 = (1 << 8) - 1
	fmt.Println(numberUint8)
	var numberUint16 uint16 = (1 << 16) - 1
	fmt.Println(numberUint16)
	var numberUint32 uint32 = (1 << 32) - 1
	fmt.Println(numberUint32)
	var numberUint64 uint64 = (1 << 64) - 1
	fmt.Println(numberUint64)
}
